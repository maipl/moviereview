<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actor extends Model
{
    public function films()
    {
        return $this->belongsToMany('App\Actor', 'actor_film', 'actor_id', 'film_id');
    }
}
