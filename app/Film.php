<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
     /**
     * Get the categories that owns the film.
     */
    public function categories()
    {
        return $this->belongsToMany('App\Category', 'category_film', 'film_id', 'category_id');
    }

    public function countries()
    {
        return $this->belongsToMany('App\Country', 'country_film', 'film_id', 'country_id');
    }

    public function actors()
    {
        return $this->belongsToMany('App\Actor', 'actor_film', 'film_id', 'actor_id');
    }

    public function ratings()
    {
        return $this->hasMany('App\Rating', 'film_id', 'id');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment', 'film_id', 'id');
    }

    public function scopeCategoryFilms(){
        return $this->has('categories')->where('id',1)->get();
    }

    public function scopeNewFilmsLimit(){
        return $this->orderBy('id','desc')->take(4)->get();
    }

    public function scopeHotFilmsLimit(){
        return $this->orderBy('view','desc')->take(4)->get();
    }

    public function scopeBestFilmsLimit(){
        return $this->orderBy('point','desc')->take(4)->get();
    }

    public function scopeNewFilms(){
        return $this->orderBy('id','desc')->take(4)->get();
    }

    public function scopeHotFilms(){
        return $this->orderBy('view','desc')->get();
    }

    public function scopeBestFilms(){
        return $this->orderBy('point','desc')->get();
    }

    public function scopeRatingPoint($film){
    }

}
