@extends('layouts.app')
@section('content')
<link href="{{ asset('css/home.css') }}" rel="stylesheet">
<div style="margin-top: -50px">   

<!-- Carousel
================================================== -->
<div id="myCarousel" class="carousel slide">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="item active">
      <!-- <img src="/assets/example/bg_suburb.jpg" style="width:100%" class="img-responsive"> -->
      <img src="http://static.hdonline.vn/i/resources/new/film/1600x600/2017/01/19/star-wars-rogue-one.jpg" style="width:100%" class="img-responsive">
      <div class="container">
        <div class="carousel-caption">
          <h3>Nơi để bạn sẻ chia những cảm xúc!</h3>
          @if (Auth::guest())
          <p><a class="btn btn-lg btn-primary" href="{{ route('register') }}">Viết đánh giá của bạn ngay bây giờ!</a>
          @endif
        </p>
        </div>
      </div>
    </div>
    <div class="item">
      <img src="http://static.hdonline.vn/i/resources/new/film/1600x600/2017/03/18/logan-2017.jpg" class="img-responsive">
      <div class="container">
        <div class="carousel-caption">
          <h3>Nơi mang đến cho bạn những lựa chọn thú vị nhất</h3>
          @if (Auth::guest())
          <p><a class="btn btn-large btn-primary" href="{{ route('register') }}">Viết đánh giá của bạn ngay bây giờ!</a></p>
          @endif
        </div>
      </div>
    </div>
    <div class="item">
      <img src="http://placehold.it/1500X500" class="img-responsive">
      <div class="container"> 
        <div class="carousel-caption">
          <h3>Một thế giới phim từ khắp nơi trên thế giới</h3>
          <p><a class="btn btn-large btn-primary" href="#">Viết đánh giá của bạn ngay bây giờ!</a></p>
        </div>
      </div>
    </div>
  </div>
  <!-- Controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="icon-prev"></span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="icon-next"></span>
  </a>  
</div>
<!-- /.carousel -->


<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->

<div class="container marketing">
  
  <div class="category-title"><a href="{{ url('/phim-moi') }}">PHIM MỚI ></a></div>
  <div class="row">
    @foreach($new_films as $film)
    @include('layouts.card_film')
    @endforeach    
  </div><!-- /.row -->

  <div class="category-title"><a href="{{ url('/phim-hot') }}">PHIM HOT ></a></div>
  <div class="row">
    @foreach($hot_films as $film)
    @include('layouts.card_film')
    @endforeach    
  </div><!-- /.row -->

  <div class="category-title"><a href="{{ url('/phim-hay') }}">PHIM HAY ></a></div>
  <div class="row">
    @foreach($best_films as $film)
    @include('layouts.card_film') 
    @endforeach    
  </div><!-- /.row -->
</div><!-- /.container -->
</div>
@endsection
