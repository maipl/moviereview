<?php 
   if($film->ratings->count()==0) $rating_point = 0;
        else{
            $rating_point = 0;
            foreach ($film->ratings as $rating) {
                $rating_point += $rating->rating;
            }
            $rating_point = $rating_point/$film->ratings->count();
            $rating_point = round($rating_point, 1);
        }   
 ?>

<div class="col-xs-3 col-sm-3 col-md-3	col-lg-3">
	<div class="panel panel-default card-film-shadow">
    	<a href="{{ url('/film/'.$film->id) }}"><img  class="display-center" src="{{$film->image}}"></a>
		<div class="film-title"  ><h4><a href="{{ url('/film/'.$film->id) }}"><p > {{$film->name}}</p></a></h4></div>
		<span>Point: {{$rating_point}} ({{$film->ratings->count()}} votes)</span>
		<p><a class="btn btn-default display-center" href="{{ url('/film/'.$film->id) }}">Xem chi tiết »</a></p>
	</div>
</div>


