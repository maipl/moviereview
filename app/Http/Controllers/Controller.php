<?php

namespace App\Http\Controllers;
use DB;
use View;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;


class Controller extends BaseController

{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function __construct(){
    	$_categories = DB::table('categories')->get();
    	$_countries = DB::table('countries')->get();
        View::share(['_categories'=>$_categories, '_countries'=>$_countries]);
    }
}
