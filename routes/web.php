<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/authentication', 'HomeController@index');
Route::get('hot', 'HomeController@hotFilms');
Route::get('/new', 'HomeController@newFilms');
Route::get('/best', 'HomeController@bestFilms');

Route::get('/film/{id}', 'FilmController@show');
Route::post('/film/{id}', 'FilmController@commentAndRatings');

Route::get('/category/{id}', 'FilmController@filterByCategory');
Route::get('/country/{id}', 'FilmController@filterByCountry');
Route::get('/actor/{id}', 'FilmController@filterByActor');