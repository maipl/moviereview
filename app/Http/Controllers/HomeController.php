<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Country;
use App\Film;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $films_category = Film::has('categories')->where('id',1)->get(); 
        $new_films = Film::newFilmsLimit();
        $hot_films = Film::hotFilmsLimit();
        $best_films = Film::bestFilmsLimit();
        $films_country = Country::find(1)->films;
        return view('home')->with([
            'new_films'=>$new_films,
            'hot_films'=>$hot_films,
            'best_films'=>$best_films,
            ]);
    }
    public function hotFilms()
    {
        $hot_films = Film::hotFilms();
        return view('list_film')->with([
            'films'=>$hot_films,
         ]);
    }
    public function newFilms()
    {
        $new_films = Film::newFilms();
        return view('list_film')->with([
            'films'=>$new_films,
         ]);
    }
    public function bestFilms()
    {
        $best_films = Film::bestFilms();
        return view('list_film')->with([
            'films'=>$best_films,
         ]);
    }
}
