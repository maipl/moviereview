@extends('layouts.app')
@section('content')
@foreach($films as $film)
<div class="col-md-10 col-md-offset-1">
	<div class="panel panel-default card-film-shadow">
		<div class="row">
			<div class="col-md-4">
				<img src="{{$film->image}}">	
			</div>
			<div class="col-md-8">
				<h2><a href="{{ url('/film/'.$film->id) }}"> {{$film->name}}</a></h2>
				<?php 
				if($film->ratings->count()==0) $rating_point = 0;
				else{
					$rating_point = 0;
					foreach ($film->ratings as $rating) {
						$rating_point += $rating->rating;
					}
					$rating_point = $rating_point/$film->ratings->count();
					$rating_point = round($rating_point, 1);
				}   
				?>
				<p><strong>Point: {{$rating_point}}</strong></p>
				<p><strong>Đạo diễn:</strong> {{$film->director}}</p>
				<p><strong>Số tập:</strong> {{$film->episode}}</p>
				<p><strong>Thời gian:</strong> {{$film->time}}</p>
				<p><strong>Năm:</strong> {{$film->year}}</p>
				<p><strong>Lượt xem:</strong> {{$film->view}}</p>
			</div>
		</div>
	</div>
</div>
@endforeach
@stop