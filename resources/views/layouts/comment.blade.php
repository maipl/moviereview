<!-- CSRF Token -->
<meta name="_token" content="{{ csrf_token() }}">
<link href="{{ asset('css/comment.css') }}" rel="stylesheet"> 
<div value="{{Auth::user()->name}}" id="current-user-name"></div>
<div class="row">
	<div class="col-md-12">
		<div class="widget-area no-padding blank">
			<div class="status-upload">
				<textarea class="col-md-12" id="text-comment" rows="3" placeholder="add a review..."></textarea>
				<button class="btn btn-success green" id="btn-comment"><i class="fa fa-share"></i> review</button>
			</div><!-- Status Upload  -->
		</div><!-- Widget Area -->
	</div>
</div>

<!-- Contenedor Principal -->
<div class="comments-container">
	<ul id="comments-list" class="comments-list">
		<div id="new-comment"></div>
		@if($film->comments->count()>0)

		@foreach($film->comments as $comment)
		<li ">
			<div class="comment-main-level">
				<!-- Avatar -->
				<div class="comment-avatar"><img src="http://placehold.it/65x65" alt=""></div>
				<!-- Contenedor del Comentario -->
				<div class="comment-box">
					<div class="comment-head">
						<h6 class="comment-name"><a>{{$comment->user->name}}    </a><span>{{$comment->created_at}}</span></h6>
						
						<i class="fa fa-reply"></i>
						<i class="fa fa-heart"></i>
						@if($comment->user->id == Auth::user()->id)
						<i class="fa fa-trash" id="delete-comment" value="{{$comment->id}}"></i>
						@endif
					</div>
					<div class="comment-content">
						{{$comment->comment}}
					</div>
				</div>
			</div>
		</li>
		@endforeach
		@endif

	</ul>
</div>
