@extends('layouts.app')
@section('content')
<link href="{{ asset('css/film.css') }}" rel="stylesheet">

<div class="container">
		<div class="card">
			<div class="container-fliud">
				<div class="wrapper row">
					<div class="col-md-6 ">
						 		<img class="display-center" src="{{$film->image}}">
					</div>
					<div class="details col-md-6">
						<h3 class="film-title">{{$film->name}}      ( <span id="point">{{$rating_point}}</span>)</h3>
						<div class="rating">
						@if (Auth::user())
						<?php $i = 0 ?>
							@if($film->ratings->count() > 0)
								@foreach($film->ratings as $rating)
									@if($rating->user == Auth::user()) 
										<h4><div id="hearts-existing" class="starrr" data-rating='{{$rating->rating}}'></div></h4>
										You gave a rating of <span id="count">{{$rating->rating}}</span> star(s)
										<?php $i=1 ; ?>
									@endif
								@endforeach
								@if($i===0)
								<h4><div id="hearts" class="starrr"></div></h4>
								You gave a rating of <span id="count-existing">~</span> star(s)
								@endif
							@endif
						@endif

						
								
							<div class="stars">
								<span>({{$film->point}})</span>
							</div>
							<span class="review-no"> {{$film->comments->count()}} reviews, <span id="vote">{{$film->ratings->count()}}</span> vote</span>
						</div>
						<span>
						<strong>Thể loại:</strong>
							@if($film->categories->count()>0)
								@foreach($film->categories as $cate)
									<a href="{{url('/category/'.$cate->id)}}">{{$cate->name}},</a>
								@endforeach 
							@endif
						</span>
						<span>
						<strong>Quốc gia:</strong>
							@if($film->countries->count()>0)
								@foreach($film->countries as $country)
									<a href="{{url('/country/'.$country->id)}}">{{$country->name}},</a>
								@endforeach 
							@endif
						</span>
						<span>
						<strong>Diễn viên:</strong>
							@if($film->actors->count()>0)
								@foreach($film->actors as $actor)
									<a href="{{url('/actor/'.$actor->id)}}">{{$actor->name}},</a>
								@endforeach 
							@endif

						</span>
						<span><strong>Đạo diễn:</strong> {{$film->director}}</span>
						<span><strong>Số tập:</strong> {{$film->episode}}</span>
						<span><strong>Thời gian:</strong> {{$film->time}}</span>
						<span><strong>Năm:</strong> {{$film->year}}</span>
						<span><strong>Lượt xem:</strong> {{$film->view+1}}</span>
						<span><strong>Giới thiệu:</strong> {{$film->description}}</span>



						<!-- <h4 class="price">current price: <span>$180</span></h4>
						<p class="vote"><strong>91%</strong> of buyers enjoyed this product! <strong>(87 votes)</strong></p> -->
					</div>
				</div>
			</div>
		</div>
		@include('layouts.comment')
	</div>
	<?php 
	$view = $film->view + 1; 
	DB::table('films')
	->where('id', $film->id)
	->update(['view' => $view]);
	?>
	@stop