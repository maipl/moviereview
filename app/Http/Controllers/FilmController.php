<?php

namespace App\Http\Controllers;
use DB;
use App\Film;
use App\Category;
use App\Country;
use Input;
use Auth;
use Carbon\Carbon;
use App\Comment;
use Illuminate\Http\Request;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Film  $film
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::find($id);
        if($film->ratings->count()==0) $rating_point = 0;
        else{
            $rating_point = 0;
            foreach ($film->ratings as $rating) {
                $rating_point += $rating->rating;
            }
            $rating_point = $rating_point/$film->ratings->count();
            $rating_point = round($rating_point, 1);
        }
        // $rating_point = Film::ratingPoint($film);
        return view('film')->with([
            'film'=>$film,
            'rating_point' => $rating_point,
         ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Film  $film
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Film  $film
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Film $film)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Film  $film
     * @return \Illuminate\Http\Response
     */
    public function destroy(Film $film)
    {
        //
    }

    public function filterByCategory($id){
        $category = Category::find($id);
        $films = $category->films;
        return view('list_film')->with([
            'films'=>$films,
         ]);
    }

    public function filterByCountry($id){
        $country = Country::find($id);
        $films = $country->films;
        return view('list_film')->with([
            'films'=>$films,
         ]);
    }

    public function commentAndRatings($id)
    {
        if($_POST['check']==3){
            $id = DB::table('comments')->where([
             'id' => $_POST['data'],
             ])->delete();

        } elseif($_POST['check']==2){
            $id = DB::table('comments')->insert([
                'user_id' => Auth::user()->id,
                'film_id' => $id,
                'comment' => $_POST['data'],
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]);
            return json_encode($id);
            response()->json(['id_result' => $id,]);
        }
        else{


            $rating = DB::table('ratings')->where(['user_id'=>Auth::user()->id, 'film_id'=> $id ])->first();
            if(is_null($rating)){
                $id = DB::table('ratings')->insert([
                    'user_id' => Auth::user()->id,
                    'film_id' => $id,
                    'rating' => $_POST['data'],
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ]);
            }
            else{
                DB::table('ratings')
                ->where(['user_id'=>Auth::user()->id, 'film_id'=> $id ])
                ->update(['rating' => $_POST['data'],
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ]);
            }
        }
    }

    // public function storecomment($id)
    // {
    //     var_dump($_POST['data']); die;
    //     $id = DB::table('comments')->insert([
    //             'user_id' => Auth::user()->id,
    //            'film_id' => $id,
    //            'comment' => $_POST['data'],
    //            ]);
    // }
}
